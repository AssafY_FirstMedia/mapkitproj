//
//  GooglePlacesManager.swift
//  MapkitProj
//
//  Created by assaf yehudai on 12/02/2021.
//

import Foundation
import GooglePlaces

class GooglePlacesManager: PlacesProviderProtocol {
    
    // MARK: - Private Propeties
    private var placesClient = GMSPlacesClient.shared()
    
    
    // MARK: - Public Methods
    func fetchPlaces(completion: @escaping PlacesPrividerCompletion) {
        
        let placeFiled: GMSPlaceField = [.name, .coordinate, .formattedAddress]
        
        placesClient.findPlaceLikelihoodsFromCurrentLocation(withPlaceFields: placeFiled) {(placeLikelihoods, error) in
            
            guard error == nil else { completion(.fail(error: error!)); return }
            if let places = placeLikelihoods?.map({ GPAnnotationObject(object: $0.place)}) {
                completion(.success(places: places))
            }
        }
    }
}
