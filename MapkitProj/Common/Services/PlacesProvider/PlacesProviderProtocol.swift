//
//  PlacesProviderProtocol.swift
//  MapkitProj
//
//  Created by assaf yehudai on 12/02/2021.
//

import Foundation
import MapKit

enum PlacesProvidreResponse {
    case success(places:[MKAnnotation])
    case fail(error: Error)
}

typealias PlacesPrividerCompletion = (PlacesProvidreResponse) -> ()

protocol PlacesProviderProtocol {
    
    func fetchPlaces(completion: @escaping PlacesPrividerCompletion)
}

