//
//  LocationProviderProtocol.swift
//  MapkitProj
//
//  Created by assaf yehudai on 14/02/2021.
//

import Foundation
import MapKit

protocol LocationProviderProtocol {
    var delegate: LocationProviderDelegate! {set get}
    func startListeningForUpdates()
}

protocol LocationProviderDelegate: class {
    
    func didUpdateHeading(newHeading: CLHeading, userCoordinates: CLLocationCoordinate2D)
    func didUpdateLocation()
}

