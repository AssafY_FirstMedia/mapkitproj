//
//  LocationProvider.swift
//  MapkitProj
//
//  Created by assaf yehudai on 14/02/2021.
//

import Foundation
import MapKit

class LocationProvider: NSObject, LocationProviderProtocol {
    
    weak var delegate: LocationProviderDelegate!
    private var locationManager: CLLocationManager!
    
    override init() {
        super.init()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.headingFilter = 10.0
        locationManager.distanceFilter = 10.0
    }
    
    func startListeningForUpdates() {
        locationManager.startUpdatingHeading()
        locationManager.startUpdatingLocation()
    }
}

extension LocationProvider: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        delegate.didUpdateLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        guard let userCoordinate = manager.location?.coordinate else { return }
        delegate.didUpdateHeading(newHeading: newHeading, userCoordinates: userCoordinate)
    }
}
