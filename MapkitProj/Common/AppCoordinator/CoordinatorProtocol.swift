//
//  CoordinatorProtocol.swift
//  MapkitProj
//
//  Created by assaf yehudai on 12/02/2021.
//

import Foundation
import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }

    func start()
}
