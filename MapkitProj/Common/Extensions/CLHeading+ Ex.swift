//
//  CLLocation+ Ex.swift
//  MapkitProj
//
//  Created by assaf yehudai on 13/02/2021.
//

import Foundation
import MapKit

extension CLHeading {
    
    private var bottomHeadingRange: CLLocationDirection {
        let bottom = magneticHeading - 22.5
        return bottom < 0 ? bottom + 360.0 : bottom
    }
    
    private var topHeadingRange: CLLocationDirection {
        let top = magneticHeading + 22.5
        return top > 360.0 ? top - 360.0 : top
    }
    
    func isHeading(to direction: CLLocationDirection) -> Bool {
        
        if topHeadingRange > bottomHeadingRange {
            return bottomHeadingRange <= direction && direction <= topHeadingRange
        }
        
        return  0 <= direction && direction <= topHeadingRange ||
                bottomHeadingRange <= direction && direction <= 360.0
    }
}

extension CLLocationCoordinate2D {
    
    func relativeDirection(to coordinate: CLLocationCoordinate2D) -> CLLocationDirection {
        
        let dx = coordinate.longitude - self.longitude
        let dy = coordinate.latitude - self.latitude
        
        guard dy != 0 else {
            if dx == 0 {return 0}
            if dx > 0 {return 90.0}
            return 270.0
        }
        let degreeAngle = abs(atan(dx/dy) * 180 / .pi)
        if dx >= 0 { return (dy > 0) ? degreeAngle : degreeAngle + 90.0 }
        return (dy > 0) ? degreeAngle + 270.0 : degreeAngle + 180.0
    }
}
