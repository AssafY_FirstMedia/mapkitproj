//
//  AnnotationObject.swift
//  MapkitProj
//
//  Created by assaf yehudai on 14/02/2021.
//

import Foundation
import MapKit
import GooglePlaces

class GPAnnotationObject: NSObject, MKAnnotation {
    
    let coordinate: CLLocationCoordinate2D
    let title: String?
    let subtitle: String?
    
    init(object: GMSPlace) {
        title = object.name
        subtitle = object.formattedAddress
        coordinate = object.coordinate
    }
}
