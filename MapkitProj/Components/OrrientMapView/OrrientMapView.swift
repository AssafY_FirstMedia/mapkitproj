//
//  OrrientMapView.swift
//  MapkitProj
//
//  Created by assaf yehudai on 14/02/2021.
//

import UIKit
import MapKit

let k_annotation_idetifier = "annotaion_identifier"

class OriientMapView: MKMapView {

    // MARK: - Private Properties
    private var viewModel: OriientMapViewModel?
    
    // MARK: - Public Methods
    func setup(with viewModel: OriientMapViewModel) {
        
        // set viewModel
        self.viewModel = viewModel
        self.viewModel?.delegate = self
        
        delegate = self
        showsUserLocation = true
        
        register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: k_annotation_idetifier)
//        zoom(to: userLocation.location?.coordinate)
    }
    
    func getPlaces() {
        viewModel?.fetchPlaces()
    }
    
    // MARK: - Private Methods
    func zoom(to userLocation: CLLocationCoordinate2D?) {
        guard let location = userLocation else { return }
        let viewRegion = MKCoordinateRegion(center: location, latitudinalMeters: 1000 , longitudinalMeters: 1000)
        setRegion(viewRegion, animated: false)
    }
}

extension OriientMapView: OriientMapViewModelDelegate {
    
    func add(_ annotations: [MKAnnotation]) {
        addAnnotations(annotations)
    }
    
    func remove(_ annotations: [MKAnnotation]) {
        removeAnnotations(annotations)
    }
    
    func updateNewHeading(with diffArray: [AnnotaionDiff]) {
        for diff in diffArray {
            let view = self.view(for: diff.annotation)
            view?.alpha = diff.isInRange ? 1 : 0.3
        }
        
        setUserTrackingMode(.followWithHeading, animated: true)
    }
}

extension OriientMapView: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard !annotation.isKind(of: MKUserLocation.self) else { return nil }
        let view = mapView.dequeueReusableAnnotationView(withIdentifier: k_annotation_idetifier, for: annotation)
        (view as? MKMarkerAnnotationView)?.markerTintColor = .orange
        (view as? MKMarkerAnnotationView)?.animatesWhenAdded = true
        return view
    }
}


