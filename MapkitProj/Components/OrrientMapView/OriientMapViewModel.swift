//
//  OriientMapViewModel.swift
//  MapkitProj
//
//  Created by assaf yehudai on 14/02/2021.
//

import Foundation
import MapKit

typealias AnnotaionDiff = (annotation: MKAnnotation, isInRange: Bool)

protocol OriientMapViewModelDelegate: class {
    func add(_ annotations: [MKAnnotation])
    func remove(_ annotations: [MKAnnotation])
    func updateNewHeading(with diffArray: [AnnotaionDiff])
}

class OriientMapViewModel {
    
    // MARK: - Proerties
    weak var delegate: OriientMapViewModelDelegate?
    private var placesProvider: PlacesProviderProtocol!
    private var locationProvider: LocationProviderProtocol!
    
    // MARK: - Model
    private var annotations: [MKAnnotation]? {
        willSet {
            if let currentAnnotations = annotations {
                delegate?.remove(currentAnnotations)
            }
        }
        didSet {
            if let newAnnotations = annotations {
                delegate?.add(newAnnotations)
            }
        }
    }
    
    // MARK: - Constructor
    init(placesProvider: PlacesProviderProtocol, locationProvider: LocationProviderProtocol) {
        
        self.locationProvider = locationProvider
        self.locationProvider.delegate = self
        self.locationProvider.startListeningForUpdates()
        
        self.placesProvider = placesProvider
    }
    
    // MARK: - Public Methods
    func fetchPlaces() {
        /*
            In general ViewModels wouldn't make network requests, and would ask for Data from
            Repository / DataManager that handles app state and persistance if needed.
         */
        placesProvider.fetchPlaces() {[weak self] resposne in
            
            switch resposne {
            
            case .success(let annotations):
                self?.annotations = annotations
            
            case .fail(let error):
                print("Current place error: \(error.localizedDescription)")
                // Could alert User insted
            }
        }
    }
}

// MARK: - LocationProviderDelegate
extension OriientMapViewModel: LocationProviderDelegate {
    
    func didUpdateHeading(newHeading: CLHeading, userCoordinates: CLLocationCoordinate2D) {
        
        guard let annotations = self.annotations else {return}
        let diffArray: [AnnotaionDiff] = annotations.map { (annotaion) -> AnnotaionDiff in
            let direction = userCoordinates.relativeDirection(to: annotaion.coordinate)
            let isInRange = newHeading.isHeading(to: direction)
            return (annotation: annotaion, isInRange: isInRange)
        }
        delegate?.updateNewHeading(with: diffArray)
    }
    
    func didUpdateLocation() {
        fetchPlaces()
    }
}
