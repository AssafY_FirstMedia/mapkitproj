//
//  ViewController.swift
//  MapkitProj
//
//  Created by assaf yehudai on 12/02/2021.
//

import UIKit
import MapKit
import GooglePlaces


class AnnotationObject: NSObject, MKAnnotation {
    
    let coordinate: CLLocationCoordinate2D
    let title: String?
    let subtitle: String?
    
    init(object: GMSPlace) {
        title = object.name
        subtitle = object.formattedAddress
        coordinate = object.coordinate
    }
}

class ViewController: UIViewController {
    
    // MARK: - Properties
    var locationManager = CLLocationManager()
    private var placesClient = GMSPlacesClient.shared()
    
    private var displayedAnnotations: [MKAnnotation]? {
        willSet {
            if let currentAnnotations = displayedAnnotations {
                mapView.removeAnnotations(currentAnnotations)
            }
        }
        didSet {
            if let newAnnotations = displayedAnnotations {
                mapView.addAnnotations(newAnnotations)
            }
        }
    }
    
    // MARK: - IBOutlets
    @IBOutlet weak var mapView: MKMapView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupMap()
        getPlaces()
    }
    
    func setupMap() {
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.headingFilter = 10.0
        locationManager.distanceFilter = 10.0
        
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        
        if let userLocation = locationManager.location?.coordinate {
            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 1000 , longitudinalMeters: 1000)
            mapView.setRegion(viewRegion, animated: false)
        }
        
        mapView.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: k_annotation_idetifier)
    }
    
    
    func getPlaces() {
        
        let places: GMSPlaceField = [.name, .coordinate, .formattedAddress]
        placesClient.findPlaceLikelihoodsFromCurrentLocation(withPlaceFields: places) { [weak self] (placeLikelihoods, error) in
            
            guard error == nil else {
                print("Current place error: \(error?.localizedDescription ?? "")")
                return
            }
            
            if let annotations = placeLikelihoods?.map({ AnnotationObject(object: $0.place)}) {
                self?.displayedAnnotations = annotations
            }
        }
    }
}

extension ViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard !annotation.isKind(of: MKUserLocation.self) else { return nil }
        
        let view = mapView.dequeueReusableAnnotationView(withIdentifier: k_annotation_idetifier, for: annotation)
        if let _view = view as? MKMarkerAnnotationView {
            
            _view.markerTintColor = .orange
//            _view.alpha = 0.3
//            
//            if let heading = mapView.userLocation.heading, let location = mapView.userLocation.location {
//                if (heading.isHeading(to: location.relativeDirection(to: annotation.coordinate))) {
//                    _view.alpha = 1
//                }
//            }
        }
        return view
    }
}

extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        mapView.setUserTrackingMode(.followWithHeading, animated: true)
        
        guard let location = manager.location else { return }
        for annotation in displayedAnnotations! {
            let view = mapView.view(for: annotation)
            let direction = location.coordinate.relativeDirection(to: annotation.coordinate)
            let isHeading = newHeading.isHeading(to: direction)
            view?.alpha = isHeading ? 1 : 0.3
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        getPlaces()
    }
}



